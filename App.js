import React from 'react'
import { Image,ScrollView,Text, View } from 'react-native'

const App = () => {
  return (
    <View>
      <Header />
      <ScrollView >
      <Thumbnail />
      <Thumbnail />
      <Thumbnail />
      <Thumbnail />
      <Thumbnail />
      <Thumbnail />
      <Thumbnail />
      <Thumbnail />
      </ScrollView>
      
    </View>
  )
}
 
const Header = () => {
  return(
    <View style={{height: 50, backgroundColor: '#FFFFFFFF', flexDirection: 'row'}}>
        <Image
          style={{"width": 100, "height": 40, marginRight: 190, marginLeft: 10, marginTop: 7}}
          source={require('./pictures/yt.png')}
        />
        <Image
          style={{"width": 29, "height": 25, marginRight: 15,marginTop: 15}}
          source={require('./pictures/lonceng.png')}
        />
        <Image
          style={{"width": 23, "height": 25, marginRight: 15,marginTop: 15}}
          source={require('./pictures/cari.png')}
         />
        <Image
          style={{"width": 25, "height": 25, marginRight: 20,marginTop: 15}}
          source={require('./pictures/user.png')}
         />
      </View> 
  )
}
const Thumbnail = () => {
  return(
    <View style={{ backgroundColor: '#FFFFFFFF',}}>
        <Image
          style={{"width":430, "height": 300, marginTop: 10, marginBottom: 7 }}
          source={require('./pictures/tum2.jpeg')}
         />
    </View>
  )
}


export default App;